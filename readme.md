[![pipeline status](https://gitlab.com/saeedtrb/simple-use-ci/badges/master/pipeline.svg)](https://gitlab.com/saeedtrb/simple-use-ci/commits/master)
[![coverage report](https://gitlab.com/saeedtrb/simple-use-ci/badges/master/coverage.svg)](https://gitlab.com/saeedtrb/simple-use-ci/commits/master)

#Simple Use CI/CD
This is a very basic project that has one static page, but it shows how to use
the environments feature of GitLab and the recently introduced dynamic
environments which can be used for Review Apps.